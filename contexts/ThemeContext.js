import React from 'react'
import { CSSReset, ThemeProvider as ChakraThemeProvider, ColorModeProvider } from '@chakra-ui/core'
import { ThemeProvider as EmotionThemeProvider } from 'emotion-theming'
import theme from '../themes/index.js'

const ThemeContextProvider = ({ children }) => {
	return (
		<ChakraThemeProvider theme={theme}>
			<ColorModeProvider value="dark">
				<EmotionThemeProvider theme={theme}>
					<CSSReset />
					{children}
				</EmotionThemeProvider>
			</ColorModeProvider>
		</ChakraThemeProvider>
	)
}

export default ThemeContextProvider
